# Homework 2-3

## Установка и запуск через CMD.

1. cd <Путь к папке>
1. docker build -t calculator-app .
1. docker run -it -p 4000:4000 calculator-app
## Описание.
Calc.py работает на 4000 порту.

Запросы осуществляются в виде: http://127.0.0.1:4000/{OPERANDUM}?a={NUMBER}&b={NUMBER}
{OPERANDUM} - операнд.
Суммирование: sum
Вычитание: sum
Деление: div
Умножение: mult
{NUMBER} - число.

## Пример работы.
![Alt text](https://user-images.githubusercontent.com/147157811/273198872-df4e3fc6-6207-4d42-a748-e4eaee7f37be.png)

![Alt text](https://user-images.githubusercontent.com/147157811/273198345-d14b05e3-e318-409c-aa5d-8c50413ea8ad.png)

![Alt text](https://user-images.githubusercontent.com/147157811/273198392-caff91c2-b75a-496c-ba12-c6bdd0104073.png)

## Микросервис развернут на тестовой инфраструктуре CI/CD.
1. Пайпалйн включает в себя этапы:
1. Pre-commit (анализ кода)
1. Pre-build (Предварительная сборка)
1. Build (Сборка)
1. Deploy (Развертывание)
1. Test (Тестирование)

![Alt text](https://i.postimg.cc/Jzyb7Frb/Screenshot-2023-11-11-025243.jpg)

## В пайпалйн встроен semgrep-sast, который остановит пайпалйн в случае обнаружение угроз.
![Alt text](https://i.postimg.cc/xCdjYwYQ/Screenshot-2023-11-11-025549.jpg)

В процессе выполнения semgrep-sast уязвимости найдены не были.
